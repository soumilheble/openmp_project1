#!/bin/bash

COMMON_DIR="./datasets/"
RESULT_DIR="./benchmark_results/"
SORT_SEL=( 2 3 )
DAT_SETS=( TEST FACEBOOK WIKI RMAT18 RMAT19 RMAT20 RMAT21 RMAT22 )
THREAD_LIST=( 1 2 4 8 16 32 )

echo "make clean"
make clean
echo "make TCOUNT=2"
make TCOUNT=2

sleep 5

for sort_typ in ${SORT_SEL[*]}
do
    for index in ${DAT_SETS[*]}
    do
	if [ $sort_typ -gt 1 ]
	then
		for thr_count in ${THREAD_LIST[*]}
		do
			echo "make clean"
			make clean
			echo "make TCOUNT=$thr_count"
			make TCOUNT=$thr_count
			OUT_FIL=$RESULT_DIR$sort_typ"_"$thr_count"_"$index".txt"
			sleep 5
			echo "./bin/main $sort_typ $COMMON_DIR$index > $OUT_FIL"
			./bin/main $sort_typ $COMMON_DIR$index > $OUT_FIL
		done
	else
		OUT_FIL=$RESULT_DIR$sort_typ"_"$index".txt"
        	echo "./bin/main $sort_typ $COMMON_DIR$index > $OUT_FIL"
		sleep 5
        	./bin/main $sort_typ $COMMON_DIR$index > $OUT_FIL
	fi
    done
done
