#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <omp.h>

#include "sort.h"
#include "edgelist.h"

#define START_IND   0
#define END_IND     1
#define LEN_IND     2

// Order edges by id of a source vertex, 
// using the Counting Sort
// Complexity: O(E + V)
void countSortEdgesBySource(struct Edge *edges_sorted, struct Edge *edges, int numVertices, int numEdges) {

    
    int i;
    int key;
    int pos;

    // auxiliary arrays, allocated at the start up of the program
    int *vertex_cnt = (int*)malloc(numVertices*sizeof(int)); // needed for Counting Sort

    for(i = 0; i < numVertices; ++i) {
        vertex_cnt[i] = 0;
    }

    // count occurrence of key: id of a source vertex
    for(i = 0; i < numEdges; ++i) {
        key = edges[i].src;
        vertex_cnt[key]++;
    }

    // transform to cumulative sum
    for(i = 1; i < numVertices; ++i) {
        vertex_cnt[i] += vertex_cnt[i - 1];
    }

    // fill-in the sorted array of edges
    for(i = numEdges - 1; i >= 0; --i) {
        key = edges[i].src;
        pos = vertex_cnt[key] - 1;
        edges_sorted[pos] = edges[i];
        vertex_cnt[key]--;
    }


    free(vertex_cnt);

}

/** Radix Sort - Serial Version **/
void radixSortEdgesBySource0(struct Edge *edges_sorted, struct Edge *edges, int numVertices, int numEdges)
{
     int i;
     int key;
     int pos;
     
     int num_radix_shift = 0;
     int max_arr_elem = numVertices-1;
     int num_digs = 0;
     
     int vertex_cnt[256];
     
     do
     {
         num_digs++;
         num_radix_shift+=8;
     }while(max_arr_elem>>num_radix_shift);
     
     for(num_radix_shift=0; num_radix_shift<(num_digs*8); num_radix_shift+=8)
     {
         for(i = 0; i < 256; i++)
         {
             vertex_cnt[i] = 0;
         }
         
         for(i = 0; i < numEdges; i++) 
         {
             key = (edges[i].src>>num_radix_shift)&0xFF;
             vertex_cnt[key]++;
         }
         
         for(i = 1; i < 256; i++) 
         {
             vertex_cnt[i] += vertex_cnt[i-1];
         }
         
         for(i = numEdges - 1; i >= 0; --i) 
         {
             key = (edges[i].src>>num_radix_shift)&0xFF;
             pos = vertex_cnt[key] - 1;
             edges_sorted[pos] = edges[i];
             vertex_cnt[key]--;
         }
         
         for(i = 0; i < numEdges; i++)
         {
             edges[i] = edges_sorted[i];
         }
     }
 }

/** Radix Sort - Parallel Version **/
void radixSortEdgesBySource1(struct Edge *edges_sorted, struct Edge *edges, int numVertices, int numEdges)
{
     omp_set_nested(1);  // En/Disable nested parallelism
     omp_set_num_threads(NUM_THREADS); // Request threads
     //omp_set_dynamic(0); //Runtime cannot dynamically assign threads
     
     int num_radix_shift = 0;
     int max_arr_elem = numVertices-1;
     int num_digs = 0;
     
     int vertex_cnt[NUM_THREADS][256];
    
     do
     {
         num_digs++;
         num_radix_shift+=8;
     }while(max_arr_elem>>num_radix_shift);
     
     for(num_radix_shift=0; num_radix_shift<(num_digs*8); num_radix_shift+=8)
     {
         #pragma omp parallel
         {
             int i;
             int key;
             int thr_id = omp_get_thread_num();
             
             for(i = 0; i < 256; i++)
             {
                 vertex_cnt[thr_id][i] = 0;
             }
             
             #pragma omp for
             for(i = 0; i < numEdges; i++) 
             {
                 key = (edges[i].src>>num_radix_shift)&0xFF;
                 vertex_cnt[thr_id][key]++;
             }
         }
         
         int i;
         int j;
         int temp_base = 0;
         for(i = 0; i < 256; i++) 
         {
             for(j=0;j<NUM_THREADS;j++)
             {
                 temp_base = temp_base + vertex_cnt[j][i];
                 vertex_cnt[0][i] = temp_base;
             }
         }
     
         int key;
         int pos;
         
         for(i =numEdges-1; i >= 0; i--) 
         {
             key = (edges[i].src>>num_radix_shift)&0xFF;
             pos = vertex_cnt[0][key] - 1;
             edges_sorted[pos] = edges[i];
             vertex_cnt[0][key]--;
         }
         
         for(i = 0; i < numEdges; i++)
         {
             edges[i] = edges_sorted[i];
         }
     }
}

/** Radix Sort - Parallel Version **/
void radixSortEdgesBySource2(struct Edge *edges_sorted, struct Edge *edges, int numVertices, int numEdges)
{
    omp_set_nested(1);  // En/Disable nested parallelism
    omp_set_num_threads(NUM_THREADS); // Request threads
    //omp_set_dynamic(0); //Runtime cannot dynamically assign threads
    
    int num_radix_shift = 0;
    int max_arr_elem = numVertices-1;
    int num_digs = 0;
    
    int end_indx[NUM_THREADS][64]={{0}};
    int vertex_cnt[NUM_THREADS][256];
    
    do
    {
        num_digs++;
        num_radix_shift+=8;
    }while(max_arr_elem>>num_radix_shift);
    
    for(num_radix_shift=0; num_radix_shift<(num_digs*8); num_radix_shift+=8)
    {
        #pragma omp parallel default(none) shared(num_radix_shift,edges_sorted,edges,numEdges,vertex_cnt,end_indx)
        {
            int i;
            int j;
            int key;
            int pos;
            int temp_base = 0;
            int thr_id = omp_get_thread_num();
            
            for(i = 0; i < 256; i++)
            {
                vertex_cnt[thr_id][i] = 0;
            }
            
            #pragma omp for schedule(static)
            for(i = 0; i < numEdges; i++) 
            {
                key = (edges[i].src>>num_radix_shift)&0xFF;
                vertex_cnt[thr_id][key]++;
//                 printf("TID: %d, Index: %d\n",thr_id,i);
                end_indx[thr_id][END_IND] = i;
                end_indx[thr_id][LEN_IND]++;
            }
            end_indx[thr_id][END_IND]++;
            
            /** EXTRA PRINT */
//             printf("After Key Count, TID: %d\n",thr_id);
//             for(i=0; i<256; i++)
//             {
//                 printf("TID: %d, Key: %d, Value: %d\n", thr_id, i, vertex_cnt[thr_id][i]);
//             }
            
            #pragma omp barrier
            
            
            #pragma omp single
            for(i = 0; i < 256; i++) 
            {
                for(j=0;j<NUM_THREADS;j++)
                {
                    temp_base = temp_base + vertex_cnt[j][i];
                    vertex_cnt[j][i] = temp_base;
                }
            }
            
            #pragma omp single
            {
                end_indx[0][START_IND]=0;
                for(i=1;i<NUM_THREADS;i++)
                {
                    end_indx[i][START_IND] = end_indx[i-1][END_IND];
                }
            }
            
            /** EXTRA PRINT */
//             printf("After Combining Keys, TID: %d, Start: %d, End: %d, Len: %d\n",thr_id,end_indx[thr_id][START_IND],end_indx[thr_id][END_IND],end_indx[thr_id][LEN_IND]);
//             for(i=0; i<256; i++)
//             {
//                 printf("TID: %d, Key: %d, Value: %d\n", thr_id, i, vertex_cnt[thr_id][i]);
//             }
            
            #pragma omp barrier
            
            #pragma omp for schedule(static)
            for(i = 0; i < numEdges; i++) 
            {
                key = (edges[end_indx[thr_id][END_IND]-1-i+end_indx[thr_id][START_IND]].src>>num_radix_shift)&0xFF;
                pos = vertex_cnt[thr_id][key] - 1;
                edges_sorted[pos] = edges[end_indx[thr_id][END_IND]-1-i+end_indx[thr_id][START_IND]];
                vertex_cnt[thr_id][key]--;
                
//                 printf("TID: %d, Index: %d, Key: %d, Position: %d\n",thr_id,end_indx[thr_id][END_IND]-1-i+end_indx[thr_id][START_IND],key,pos);
            }
            
            #pragma omp barrier
            
            #pragma omp for schedule(static)
            for(i = 0; i < numEdges; i++)
            {
                edges[i] = edges_sorted[i];
            }
        }
    }
}
