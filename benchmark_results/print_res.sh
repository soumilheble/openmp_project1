FIL_LIST=( TEST FACEBOOK WIKI RMAT18 RMAT19 RMAT20 RMAT21 RMAT22 )
SORT_TYPE=( 0 1 2 3 )
THR_COUNT=( 1 2 4 8 16 32 )

for file_lst in ${FIL_LIST[*]}
do
	for srt_typ in ${SORT_TYPE[*]}
	do
		if [ $srt_typ -gt 1 ]
		then
			for thr_cnt in ${THR_COUNT[*]}
			do
				echo $srt_typ", "$thr_cnt", "$file_lst", "$(sed -n '5p' $srt_typ"_"$thr_cnt"_"$file_lst".txt" | awk '{print $2}')
			done
		else
			echo $srt_typ", 0, "$file_lst", "$(sed -n '5p' $srt_typ"_"$file_lst".txt" | awk '{print $2}')
		fi
	done
done
